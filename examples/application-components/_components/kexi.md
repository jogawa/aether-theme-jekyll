---
layout: component
name: Kexi
shortDescription: >
  Database creation for everyone
description: >
  Kexi is a visual database applications creator. It can be used for designing database applications, inserting and editing data, performing queries, and processing data. Forms can be created to provide a custom interface to your data. All database objects – tables, queries, forms, reports – are stored in the database, making it easy to share data and design.
screenshot: /assets/img/kexi-summary.png
css-include: /css/component.css
redirect_from:
  - /kexi/screenshots/
  - /kexi/screenshots/2_5/
  - /kexi/screenshots/2_6/
---

Kexi is a visual database applications creator. It can be used for designing database applications, inserting and editing data, performing queries, and processing data. Forms can be created to provide a custom interface to your data. All database objects – tables, queries, forms, reports – are stored in the database, making it easy to share data and design.

Kexi is considered as a long awaited Open Source competitor for Microsoft Access, FileMaker and Oracle Forms. Its development is motivated by the lack of Rapid Application Development (RAD) tools for database systems that are sufficiently powerful, inexpensive, open standards driven and portable across many operating systems and hardware platforms.

Kexi is Free/Libre/Open-Source Software. As a real member of the KDE and Calligra projects,Kexi integrates fluently into both. It is designed to be fully usable while running outside of the KDE Plasma, so it can run on Linux/BSD/Unix e.g. under the GNOME desktop, on macOS, and on MS Windows.

## See also

- [Kexi home page](http://www.kexi-project.org)
- [Kexi forums](http://forum.kde.org/kexi)
- [Kexi Handbook](http://userbase.kde.org/Kexi/Handbook)
- [Kexi Info, Tutorials and Samples](http://userbase.kde.org/Kexi)

## Screenshots



<center> Startup dialog for a new Kexi project </center> <br/>



<center> Kexi Table View storing images </center> <br/>


<center>Kexi Form </center> <br/>


<center>Kexi Form Design View </center> <br/>


<center>Kexi - Assigning Actions </center> <br/>


<center>Kexi Report </center> <br/>


<center>Kexi Report Design View </center>


<center>Kexi Advanced CSV Export Dialog </center> <br/>


<center>Kexi SQL Support </center>